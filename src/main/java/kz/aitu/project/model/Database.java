package kz.aitu.project.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    final String HOST = "jdbc:postgresql://localhost:5432/postgres";
    final String USERNAME = "postgres";
    final int PASSWORD = 123;

    public Connection getConnection() {
        return connection;
    }

    private Connection connection;
    public Database() {
        try {
            connection = DriverManager.getConnection(HOST, USERNAME, String.valueOf(PASSWORD));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
