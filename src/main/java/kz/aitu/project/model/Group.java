package kz.aitu.project.model;

import lombok.Data;

@Data
public class Group {
    private int id;
    private String name;


    public  Group(){}
    public Group(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString(){
        return String.format("Group id is " + id + ", group name is " + name);
    }
}
