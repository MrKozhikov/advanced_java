package kz.aitu.project.model;

import lombok.Data;

@Data
public class Student {
    private int id;
    private String name;
    private String phone;
    private int groupId;

    public  Student(){}

    public Student(int id, String name, String phone, int groupId) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.groupId = groupId;
    }

    @Override
    public String toString(){
        return String.format("Student id is " + id + ", name is " + name + ", phone is " + phone + ", group id is " + groupId );
    }
}
