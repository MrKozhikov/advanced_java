package kz.aitu.project;

import kz.aitu.project.model.Database;
import kz.aitu.project.model.Group;
import kz.aitu.project.model.Student;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Main {
    public static void main(String[] args) {
        Group group = new Group();
        Student student = new Student();
        Database database = new Database();

        String query = "select  * from student";
        try {
            Statement statement = database .getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                student.setId(resultSet.getInt(4));
                student.setName(resultSet.getString(1));
                student.setPhone(resultSet.getString(2));
                student.setGroupId(resultSet.getInt(3));
                System.out.println(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("\n");
        String second_query = "select  * from university_group";
        try {
            Statement statement = database .getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(second_query);
            while (resultSet.next()) {
                group.setName(resultSet.getString(1));
                group.setId(resultSet.getInt(2));
                System.out.println(group);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
